import examples from './examples';

export default {
  followsDesignSystem: true,
  examples,
};
