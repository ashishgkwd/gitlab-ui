### Default card

A card is a flexible and extensible content container. It includes options for headers and footers, a wide variety of content, contextual background colors, and powerful display options.

[`<gl-card>`]: https://gitlab.com/gitlab-org/gitlab-ui/blob/master/documentation/card.md