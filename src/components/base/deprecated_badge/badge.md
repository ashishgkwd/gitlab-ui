# GlDeprecatedBadge

<!-- STORY -->

> Note: `GlDeprecatedBadge` is _deprecated_ and should not be used. It exists only to ease migration to `GlBadge`, which should be used instead.
